# Introduction à GIT recap.

L'introduction à GIT de ce matin était une première rencontre avec un outil de gestion de soucre moderne. Commencer à s'en servir est un effort. Mais l'effort vaut la peine.
why git ?
- garder un historique des modification apportées à une base de code.
- travailler sur différentes fonctionnalitées du code sans impacter d'autre personnes.
- sauvegarder son code en cas de panne matérielle.
- Garder et maintenir plusieur version du même code a destination de différentes personnes / environnements.
- etc....

Cette introduction vous permet de commencer à utiliser GIT sur vos projet. **MAIS** il sera nécessaire d'approfondir votre connaissance de l'outil à l'usage pour bénéficier de tous les avantages qu'il apporte.

Avec Git le markdown et un outil de templating pour la documentation. 

> Git et le markdown sont utilisé dans la plupart des projet moderne. Maitriser ces outils est loin d'être une perte de temps que vous soyez un dev ou un scientifique.

## Ressources

`cheat sheets`

GIT
- https://education.github.com/git-cheat-sheet-education.pdf
- https://www.atlassian.com/dam/jcr:e7e22f25-bba2-4ef1-a197-53f46b6df4a5/SWTM-2088_Atlassian-Git-Cheatsheet.pdf

Markdown
- https://commonmark.org/help/ Avec un petit tuto sur le markdown en general.

`ressources vues pendant la formation` 

Vaut le coup d'être lu/survolé au moins une fois pour savoir ou trouver des infos si vous en avez besoin.

Des diagrammes pour comprendre les commandes de base
https://marklodato.github.io/visual-git-guide/index-en.html

Un outil visuel des commandes de base avec lequel jouer.
https://onlywei.github.io/explain-git-with-d3

 ⚠️**Une liste de recettes si vous avez fait une erreur et ne savez pas comment la corriger**
http://ohshitgit.com

## Worflow simple et conventions

`conventions`

- Nommez vos commits de façon claire et en Anglais
**DO:**
"Added test to generateCurrent function"
"Refactored parseAis function"
"Fixed NULL when calling openOrbcomFile function"
**DONT**
"WIP"
"test"
"modif"

- Nommez vos branches de façon explicite en Anglais et prefixez avec le tip d'action effecturée.
*feature-add-current-generation-module*
*bugfix-check-null-pointer-when-calling-current-generation-function*

- Créez une nouvelle branche pour developper une nouvelle fonction de votre code. 
Cela permet de garder votre branche master propre avec du code qui fonctionne. 
Une fois la fonction finie. Mergez la branche feature sur votre branche master.
Si des modifications sont apportées par d'autres personnes sur master. Pensez à pull la branche origin/master sur votre master. puis *rebase* votre branche de dev sur master AVANT de merge votre branche feature sur master.

example:
```git
# creates and checkout a new branch
git branch -b "feature-parse-orbcom-ais-files"

# add multiple explicit commit on feature branch
git commit -m "explicit commit name 1"
git commit -m "explicit commit name 2"
git commit -m "explicit commit name 3"

# If other people are working on the project
git checkout master
git pull
git checkout feature-branch-name
# replay master modification on feature branch
git rebase master

# Merge feature branch modification to master
git checkout master
git merge feature-branch-name

```

## Au final

Il y a énormément de choses que je n'ai pas abordées. Sinon on aurait eu 2 jours de formation. Je pense qu'il faudra faire un rappel voir un petite formation avancée.

Si vous avez la moindre question envoyez moi un message sur slack.

Créez vous un compte sur https://gitlab.com/  ou https://github.com/ et essayez de mettre un de vos projets sur le site (en private repo).

`Random Tip:`

**TIP 1**

Si vous avez des données dans un dossier de travail que vous ne voulez pas synchroniser ( Eviter de gerer des fichier lourds avec git), par exemple un dossier de build de votre code. Des Fichiers AIS de 600mo. Des fichiers de configuration pour votre machines ou spécifiques à votre IDE....

Créez un fichier ".gitignore" dans le repertoire et spécifiez les fichier que git ignorera.

spécifiez les fichiers ou dossier à ignorer:
```
# ignore all xml files
*.xml

# ignore folders
/build
/target

```
Plus d'infos [.gitignore](https://www.atlassian.com/git/tutorials/saving-changes/gitignore)


**TIP 2**

Git permet de créer des alias. L'alias que vous m'avez vu utiliser pendant la formation 

```
git lola
```

Affiche l'arbre des commits / branche du repo en cours.

> git config --global alias.lola "log --graph --decorate --pretty=oneline --abbrev-commit --all"





Bonne fin de journée !

